import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-ngFor';
  persons:Array<any>=[{position:"Boss", name:"Sara", age: 20},
{position:"Secretary", name:"jary", age: 25},
{position:"Secretary", name:"adri", age: 20},
{position:"Employee", name:"dani", age: 30}]

}
